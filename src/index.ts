
/**
 * Merge objects past as parameter into a new one without allowing duplicates.
 * If a duplicate is found an Error is thrown.
 */
export default function mergeObjectsWithoutDuplicate(...args: object[]) {
    let res = {};

    for (let i = args.length; --i >= 0;) {
        let object = args[i];
        if (object === undefined || object === null) {
            continue;
        }
        let keys = Object.keys(object);
        let duplicateKeys = Object.keys(res).filter(x => keys.includes(x));
        if (duplicateKeys.length > 0) {
            throw new Error("Duplicate key while merging objects: " + duplicateKeys);
        }
        res = {...res, ...object};
    }
    return res;
}
