import mergeObjectsWithoutDuplicate from '../src';

const objectEmpty1 = {}
const objectEmpty2 = {}

const object1 = {
    key1: "toto",
    key2: "titi",
    key3: "tata"
}
const object2 = {
    key2: "toto",
    key3: "titi",
    key4: "tata"
}
const object3 = {
    key4: "toto",
    key5: "titi",
    key6: "tata"
}


test('merge basic', () => {
    expect(mergeObjectsWithoutDuplicate({}, object1)).toStrictEqual(object1);
    // @ts-ignore
    expect(mergeObjectsWithoutDuplicate(null, object1)).toStrictEqual(object1);
    // @ts-ignore
    expect(mergeObjectsWithoutDuplicate(undefined, object1)).toStrictEqual(object1);
    expect(mergeObjectsWithoutDuplicate(objectEmpty1, object1)).toStrictEqual(object1);
    expect(mergeObjectsWithoutDuplicate(object1, object3)).toStrictEqual({
        key1: "toto",
        key2: "titi",
        key3: "tata",
        key4: "toto",
        key5: "titi",
        key6: "tata"
    });
});


test('merge (with empty object)', () => {
    expect(mergeObjectsWithoutDuplicate(objectEmpty1, objectEmpty2)).toStrictEqual({});
});


test('merge (with null)', () => {
    // @ts-ignore
    expect(mergeObjectsWithoutDuplicate(objectEmpty1, null)).toStrictEqual({});
});


test('merge (with undefined)', () => {
    // @ts-ignore
    expect(mergeObjectsWithoutDuplicate(objectEmpty1, undefined)).toStrictEqual({});
});


test('merge (with duplicated key)', () => {

    expect(() => mergeObjectsWithoutDuplicate(object1, object2))
        .toThrowError(new Error("Duplicate key while merging objects: key2,key3"));
});
