# Merge Objects without duplicate
Merge objects together without allowing duplicates.

## Issues/Bug report or improvement ideas
https://gitlab.com/olive007/case-convert/-/issues

## License
GNU Lesser General Public License v3 or later (LGPLv3+)
